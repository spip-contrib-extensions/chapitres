<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'chapitres_description' => 'Ce plugin permet d\'ajouter des contenus chapitrés à n\'importe quel autre contenu. On configure sur quels contenus on veut activer les chapitres, par exemple sur les articles, ou autre, et ensuite sur les pages d\'admin de ces contenus on peut ajouter des chapitres. Ils peuvent être arborescents, on peut donc créer des chapitres dans des chapitres.',
	'chapitres_nom' => 'Chapitres',
	'chapitres_slogan' => 'Ajouter des chapitres à n\'importe quel contenu.',
);
