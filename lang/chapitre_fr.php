<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_chapitre' => 'Ajouter ce chapitre',

	// B
	'bouton_plier' => 'Plier',
	'bouton_deplier' => 'Déplier',

	// C
	'champ_chapo_label' => 'Chapeau',
	'champ_id_parent_label' => 'Chapitre parent',
	'champ_texte_label' => 'Texte',
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_chapitre' => 'Confirmez-vous la suppression de ce chapitre ?',

	// I
	'icone_creer_chapitre' => 'Créer un chapitre',
	'icone_creer_chapitre_enfant' => 'Créer un sous-chapitre',
	'icone_ajouter_chapitre_cousin' => 'Ajouter un chapitre au même niveau',
	'icone_ajouter_chapitre_enfant' => 'Ajouter un sous-chapitre',
	'icone_modifier_chapitre' => 'Modifier ce chapitre',
	'info_1_chapitre' => 'Un chapitre',
	'info_aucun_chapitre' => 'Aucun chapitre',
	'info_chapitres_auteur' => 'Les chapitres de cet auteur',
	'info_nb_chapitres' => '@nb@ chapitres',
	'info_1_chapitre_enfant' => 'Un sous-chapitre',
	'info_aucun_chapitre_enfant' => 'Aucun sous-chapitre',
	'info_nb_chapitres_enfants' => '@nb@ sous-chapitres',

	// R
	'retirer_lien_chapitre' => 'Retirer ce chapitre',
	'retirer_tous_liens_chapitres' => 'Retirer tous les chapitres',

	// S
	'supprimer_chapitre' => 'Supprimer ce chapitre',

	// T
	'texte_ajouter_chapitre' => 'Ajouter un chapitre',
	'texte_changer_statut_chapitre' => 'Ce chapitre est :',
	'texte_creer_associer_chapitre' => 'Créer et associer un chapitre',
	'texte_definir_comme_traduction_chapitre' => 'Ce chapitre est une traduction du chapitre numéro :',
	'titre_chapitre' => 'Chapitre',
	'titre_chapitres' => 'Chapitres',
	'titre_chapitres_sous' => 'Sous-chapitres',
	'titre_chapitres_rubrique' => 'Chapitres de la rubrique',
	'titre_langue_chapitre' => 'Langue de ce chapitre',
	'titre_logo_chapitre' => 'Logo de ce chapitre',
	'titre_objets_lies_chapitre' => 'Liés à ce chapitre',
	'titre_plan' => 'Plan des chapitres',
	'titre_vue_enfants' => 'Liste',
	'titre_vue_editables' => 'Édition',
);
