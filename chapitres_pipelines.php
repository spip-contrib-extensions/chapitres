<?php
/**
 * Utilisations de pipelines par Chapitres
 *
 * @plugin     Chapitres
 * @copyright  2018
 * @author     Les Développements Durables
 * @licence    GNU/GPL
 * @package    SPIP\Chapitres\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Ajout de contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function chapitres_affiche_milieu($flux) {
	$texte = '';

	// auteurs sur les chapitres
	if ($e = trouver_objet_exec($flux['args']['exec']) and !$e['edition'] and in_array($e['type'], array('chapitre'))) {
		$texte .= recuperer_fond('prive/objets/editer/liens', array(
			'table_source' => 'auteurs',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']]
		));
	}

	if ($texte) {
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}

/**
 * Ajout de contenu sous la fiche d'un objet
 *
 * => Chapitres enfants pour les objets configurés et les chapitres
 *
 * @pipeline affiche_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function chapitres_affiche_enfants($flux) {
	include_spip('inc/config');
	$objets = lire_config('chapitres/objets', array());

	if (
		isset($flux['args']['objet'])
		and isset($flux['args']['id_objet'])
		and $objet = $flux['args']['objet']
		and $id_objet = intval($flux['args']['id_objet'])
		and (
			in_array(table_objet_sql($objet), $objets)
			or $objet == 'chapitre'
		)
	) {
		$enfants = recuperer_fond(
			'prive/objets/contenu/chapitre-enfants-ajax',
			array(
				'objet'       => $objet,
				'id_objet'    => $id_objet,
				'chapitres'   => _request('chapitres'), // type de vue des chapitres
				'id_chapitre' => _request('id_chapitre'), // pour exposer
			),
			array (
				'ajax' => 'chapitres',
			)
		);

		$flux['data'] .= $enfants;
	}

	return $flux;
}

/**
 * Ajout de contenu dans la colonne de gauche
 *
 * => Plan des chapitres pour les objets configurés et les chapitres
 *
 * @pipeline affiche_gauche
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function chapitres_afficher_config_objet($flux) {

	include_spip('inc/config');
	$objets = lire_config('chapitres/objets', array());

	if (
		isset($flux['args']['type'])
		and isset($flux['args']['id'])
		and $objet = $flux['args']['type']
		and $id_objet = intval($flux['args']['id'])
		and (
			in_array(table_objet_sql($objet), $objets)
			or $objet == 'chapitre'
		)
	) {

		if ($objet == 'chapitre') {
			$chapitre = sql_fetsel('objet, id_objet', 'spip_chapitres', 'id_chapitre = '.$id_objet);
			$id_chapitre = $id_objet;
			$objet       = $chapitre['objet'];
			$id_objet    = $chapitre['id_objet'];
		}

		$plan = recuperer_fond(
			'prive/squelettes/inclure/chapitres_plan',
			array(
				'objet'       => $objet,
				'id_objet'    => $id_objet,
				'id_chapitre' => $id_chapitre ?? 0, // pour exposer
				'titre'       => _T('chapitre:titre_plan'),
				'statut'      => array('prepa', 'propose', 'publie'),
				'chapitres'   => _request('chapitres'), // type de vue des chapitres
			),
			array('ajax' => 'chapitres_plan')
		);

		$flux['data'] .= $plan;
	}
	
	return $flux;
}

/**
 * Agir avant l'insertion d'un nouvel objet dans la base
 *
 * => Chapitre : définir le parent
 * => Chapitre : publier d'office éventuellement
 * 
 * @pipeline pre_insertion
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function chapitres_pre_insertion($flux) {

	if ($flux['args']['table'] == 'spip_chapitres') {

		include_spip('inc/config');

		// 1) Définir le parent
		// S'il y a un id_parent
		if ($id_parent = intval($flux['data']['id_parent'])
			or $id_parent = intval($flux['args']['id_parent'])
		) {
			$flux['data']['id_parent'] = $id_parent;
			
			// Et dans ce cas, le nouveau chapitre utilise forcément l'objet et id_objet du parent
			$parent = sql_fetsel('objet, id_objet', 'spip_chapitres', 'id_chapitre = '.intval($id_parent));
			$flux['data']['objet'] = $parent['objet'];
			$flux['data']['id_objet'] = intval($parent['id_objet']);
		}
		// Sinon il y a peut-être l'objet parent à remplir quand même
		elseif ($objet = _request('objet') and $id_objet = intval(_request('id_objet'))) {
			$flux['data']['objet'] = $objet;
			$flux['data']['id_objet'] = $id_objet;
		}

		// 2) Publier éventuellement
		if (lire_config('chapitres/publier_auto')) {
			$flux['data']['statut'] = 'publie';
		}

	}
	
	return $flux;
}


/**
 * Agir avant l'édition d'un objet
 *
 * => Modification / institution d'un chapitre : si id_parent a été modifié, le renvoyer dans la liste des champs sinon il est ignoré.
 * l'API cherche par défaut une rubrique comme parent, qui forcément n'existe pas.
 *
 * @pipeline pre_edition
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function chapitres_pre_edition($flux) {
	// Si on modifie ou institue un chapitre et qu'un nouveau id_parent est sélectionné
	if (
		include_spip('base/objets')
		and objet_type($flux['args']['table']) == 'chapitre'
		and in_array($flux['args']['action'], array('instituer', 'modifier'))
		and $id_chapitre = intval($flux['args']['id_objet'])
		and !is_null($id_parent = _request('id_parent'))
		and (($id_parent_ancien = sql_getfetsel('id_parent', 'spip_chapitres', 'id_chapitre='.intval($id_chapitre))) !== false)
		and $id_parent != $id_parent_ancien
	) {

		$flux['data']['id_parent'] = intval($id_parent);

	}

	return $flux;
}


/**
 * Optimiser la base de données
 *
 * Supprime les objets à la poubelle.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function chapitres_optimiser_base_disparus($flux) {

	sql_delete('spip_chapitres', "statut='poubelle' AND maj < " . sql_quote(trim($flux['args']['date'], "'")));

	return $flux;
}
